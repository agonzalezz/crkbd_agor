syntax enable

set mouse=a
set encoding=utf-8  " Codification
set showmatch       " When over one parenthesis,highlightshow its pair
set ruler           " Show cursor position, bar bottom right
set showcmd         " Show the command in the left right
set guicursor=
set nohlsearch      " do not highlight every occurrence of searched word
set hidden          " hide msg E37: No write since last change (add ! to override)
set noerrorbells
set sw=2            " 2 space instead of tabs
set shiftwidth=4
set expandtab       " Tab inserts spaces
set number          " Show line numbers
set numberwidth=1   " Margin left, where the ruller is
set tabstop=2       " How many columns a tab counts for
set softtabstop=0   " how many columns vim uses when you hit Tab in insert mode.
set expandtab       " tab key insert spaces instead of tabs
set shiftwidth=2    " size of indent
set smartindent
set nowrap          " dont wrap lines
set smartcase       " case-insensitive search with /
set noswapfile      " no backups
set nobackup        " no backups
set termguicolors   " neovim: Enables 24-bit RGB color in the |TUI|
set scrolloff=8     " always keep 8 lines above and below the cursor
set noshowmode      " hides mode of operation
set laststatus=2    " always show the last status
set incsearch       " enable incremental seach. when searching with / highlights on typing
set undofile        " enable undo
set undodir=~/.vim/undodir  "directory where to save all the undo history
set cmdheight=2    "give more space for displaying messages.
set clipboard=unnamed  " Allow copy to clipboard?

" Having longer updatetime (default is 4000 ms = 4 s) leads to noticeable
" delays and poor user experience.
set updatetime=50

set shortmess=a
set backspace=indent,eol,start
set colorcolumn=80
set wildignore=*/dist*/*,*/target/*,*/builds/*,*/node_modules/* "Ignore node_modules

"set norelativenumber
"set relativenumber

" Markdown configurations
au FileType markdown setlocal textwidth=100
au FileType markdown setlocal wrap
let g:markdown_fenced_languages = ['typescript', 'ruby', 'sh', 'yaml', 'javascript', 'html', 'vim', 'coffee', 'json', 'diff']
execute pathogen#infect()


let g:molokai_original = 1
let g:rehash256 = 1
let g:python3_host_prog="/path/to/python/executable/"
highlight ColorColumn ctermbg=0 guibg=lightgrey

" Ensure vim-plug is installed
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

autocmd FileType javascript setlocal equalprg=js-beautify\ --stdin

call plug#begin('~/.vim/plugged')

let g:prettier#exec_cmd_path = "~/path/to/cli/prettier"
" post install (yarn install | npm install) then load plugin only for editing supported files
Plug 'prettier/vim-prettier', {
  \ 'do': 'yarn install',
  \ 'for': ['javascript', 'typescript', 'css', 'less', 'scss', 'json', 'graphql', 'markdown', 'vue', 'yaml', 'html','python'] }
Plug 'sbdchd/neoformat'
Plug 'beautify-web/js-beautify'
Plug 'Chiel92/vim-autoformat'
Plug 'neoclide/coc.nvim', {'branch': 'release'}  " Completion features
Plug 'tweekmonster/gofmt.vim'
Plug 'tpope/vim-fugitive'            " Git
Plug 'leafgarland/typescript-vim'    " typescript
Plug 'vim-utils/vim-man'
Plug 'mbbill/undotree'
Plug 'sheerun/vim-polyglot'          " Collection of language packs
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }  " todo: add comment
Plug 'junegunn/fzf.vim'
Plug 'stsewd/fzf-checkout.vim'       " Checkout to branches
Plug 'vuciv/vim-bujo'                " Todo list
Plug 'tpope/vim-dispatch'
"Plug 'theprimeagen/vim-be-good'
Plug 'vim-airline/vim-airline'       " Status bar at the botom

" mine
Plug 'morhetz/gruvbox'               " Retro groove theme
Plug 'jremmen/vim-ripgrep'           " fast grep
Plug 'easymotion/vim-easymotion'     " Move through text
Plug 'scrooloose/nerdtree'           " Show directory sidebar
Plug 'preservim/nerdcommenter'       " Commenter
" Plug 'git@github.com:kien/ctrlp.vim.git' " Ctrlp
"Plug 'scrooloose/nerdcommenter'
Plug 'christoomey/vim-tmux-navigator'
Plug 'w0rp/ale'                      " Asynchronous linting/fixing
"-- Plug 'chrisbra/changesPlugin'        " signal which lines have changed
"-- Plug 'mhinz/vim-signify'             " Git diff +-!
"-- Plug 'mhinz/vim-signify', { 'branch': 'legacy' }
Plug 'styled-components/vim-styled-components', { 'branch': 'main' }

" themes"
Plug 'sonph/onehalf', { 'rtp': 'vim' }
Plug 'colepeters/spacemacs-theme.vim'
Plug 'sainnhe/gruvbox-material'
Plug 'phanviet/vim-monokai-pro'
Plug 'flazz/vim-colorschemes'
Plug 'chriskempson/base16-vim'
Plug 'altercation/solarized'

" global Fin FZF
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
Plug 'wakatime/vim-wakatime'
call plug#end()

let g:gruvbox_contrast_dark = 'hard'
if exists('+termguicolors')
  let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
  let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
endif
let g:gruvbox_invert_selection='0'

" mine
colorscheme gruvbox
let NERDTreeShowHidden=1  "  Always show dot files
let NERDTreeQuitOnOpen=1

" mine, highlight with eslint, use ale
let g:ale_sign_error = '✘'
let g:ale_sign_warning = '⚠'
highlight ALEErrorSign ctermbg=NONE ctermfg=red
highlight ALEWarningSign ctermbg=NONE ctermfg=yellow
let g:ale_fixers = {
\  'javascript': ['eslint'],
\}
let g:ale_fix_on_save = 1

" Helps RG to detect your root and to search for your git root.
if executable('rg')
  let g:rg_derive_root='true'
endif

let mapleader = " "

let g:fzf_layout = { 'window': { 'width': 0.8, 'height': 0.8 } }
let $FZF_DEFAULT_OPTS='--reverse'


"mine
"when matching a whole word do not go to the next match
nnoremap * *``
nnoremap <leader>gc :GBranches<CR>
"nnoremap <leader>ga :Git fetch --all<CR>
"nnoremap <leader>grum :Git rebase upstream/master<CR>
"nnoremap <leader>grom :Git rebase origin/master<CR>
"nnorempa <leader>ghw :h <C-R>=expand("<cword>")<CR><CR>
"nnorempa <leader>prw :CocSearch <C-R>=expand("<cword>")<CR><CR>
"nnorempa <leader>pw :Rg <C-R>=expand("<cword>")<CR><CR>
"nnorempa <leader>h :wincmd h<CR>
"nnorempa <leader>j :wincmd j<CR>
"nnorempa <leader>k :wincmd k<CR>
"nnorempa <leader>l :wincmd l<CR>
"nnorempa <leader>u :UndotreeShow<CR>
nnoremap <leader>pv :wincmd v<bar> :Ex <bar> :vertical resize 30<CR>
nnoremap <Leader>ps :Rg<SPACE>
"nnorempa <C-p> :GFiles<CR>
"nnorempa <Leader>pf :Files<CR>
"nnorempa <Leader><CR> :so ~/.config/nvim/init.vim<CR>
nnoremap <Leader>+ :vertical resize +5<CR>
nnoremap <Leader>- :vertical resize -5<CR>
vnoremap J :m '>+1<CR>gv=gv
vnoremap K :m '<-2<CR>gv=gv
vnoremap X "_d
inoremap <C-c> <esc>

" closing tags
" on new line
" inoremap ><Tab> ><Esc>F<lyt>o</<C-r>"><Esc>O<Space>
" on the same line
inoremap ><Tab> ><Esc>F<lyt>o</<C-r>"><Esc>kJxi

command! -nargs=0 Prettier :CocCommand prettier.formatFile
inoremap <silent><expr> <C-space> coc#refresh()
" personal keys
nmap <leader>w :Prettier<CR>:w<CR>
nmap <leader>t :below vertical terminal<CR>
nmap <leader>q :q<CR>
nmap <leader>f :Prettier<CR>
nmap <leader>fz :FZF<CR>
" GoTo code navigation.
" mine
nmap <leader>s <Plug>(easymotion-s2)
nmap <leader>nt :NERDTreeFind<CR>
nmap <leader>gf :diffget //2 <CR>
nmap <leader>gj :diffget //3 <CR>
nmap <leader>gs :G<CR>
nmap <leader>df :Gdiff<CR>

" his
nmap <leader>gd <Plug>(coc-definition)
nmap <leader>gy <Plug>(coc-type-definition)
nmap <leader>gi <Plug>(coc-implementation)
nmap <leader>gr <Plug>(coc-references)
nmap <leader>rr <Plug>(coc-rename)
nmap <leader>g[ <Plug>(coc-diagnostic-prev)
nmap <leader>g] <Plug>(coc-diagnostic-next)
nmap <silent> <leader>gp <Plug>(coc-diagnostic-prev-error)
nmap <silent> <leader>gn <Plug>(coc-diagnostic-next-error)
nnoremap <leader>cr :CocRestart

"trim white space
fun! TrimWhitespace()
  let l:save = winsaveview()
  keeppatterns %s/\s\+$//e
  call winrestview(l:save)
endfun

":w same as :W"
com! W w

augroup highlight_yank
  autocmd!
  autocmd TextYankPost * silent! lua require'vim.highlight'.on_yank(timeuout = 200)
augroup END

augroup THE_PRIMEAGEN
  autocmd!
  autocmd BufWritePre * :call TrimWhitespace()
augroup END

" Ctrl.P
set runtimepath^=~/.vim/bundle/ctrlp.vim
let g:ctrlp_cmd = 'CtrlP'
let g:ctrlp_match_window_bottom = 0  " Ctrl P on the top rather than bottom
let g:ctrlp_working_path_mode = 'ar' " Smart path mode
let g:ctrlp_mru_files = 1            " Enable Most Recently Used files feature
let g:ctrlp_jump_to_buffer = 2       " Jump to tab AND buffer if already open
let g:ctrlp_custom_ignore = 'node_modules\|DS_Store\|git'
let g:ctrlp_user_command = ['.git/', 'git --git-dir=%s/.git ls-files -oc --exclude-standard']
let g:ctrlp_winsize = 25


let g:netrw_browse_split = 2
let g:netrw_banner = 0
let g:netrw_winsize = 25

" comment lines with ++
let g:NERDCompactSexyComs = 1      " Use compact syntax for prettified multi-line comments
let g:NERDDefaultAlign = 'left'    " Align line-wise comment delimiters flush left instead of following code indentation
let g:NERDSpaceDelims = 1          " Add spaces after comment delimiters by default
vmap ++ <plug>NERDCommenterToggle
nmap ++ <plug>NERDCommenterToggle

nmap <C-S> <Plug>BujoAddnormal
imap <C-S> <Plug>BujoAddinsert

nmap <C-Q> <Plug>BujoChecknormal
imap <C-Q> <Plug>BujoCheckinsert

" remap in insert mode to move while holding Ctrl+o
" :imap <C-h> <C-o>h
" :imap <C-j> <C-o>j
" :imap <C-k> <C-o>k
" :imap <C-l> <C-o>l


"==============================================================================
" closepairs.vim - Auto closes pairs of characters
"==============================================================================
"
" Author:   NoWhereMan (Edoardo Vacchi) <uncommonnonsense at gmail dot com>
" Version:  0.1
" URL: 	    http://www.flatpress.org/
" License:  Public Domain
"==============================================================================


inoremap ( ()<left>
inoremap { {}<left>
inoremap [ []<left>

vnoremap <leader>" "zdi"<c-r>z"
vnoremap <leader>' "zdi'<c-r>z'
vnoremap <leader>( "zdi(<c-r>z)
vnoremap <leader>[ "zdi[<c-r>z]
vnoremap <leader>{ "zdi{<c-r>z}

inoremap <expr> <bs> <SID>delpair()

inoremap <expr> ) <SID>escapepair(')')
inoremap <expr> } <SID>escapepair('}')
inoremap <expr> ] <SID>escapepair(']')

inoremap <expr> " <SID>pairquotes('"')
inoremap <expr> ' <SID>pairquotes("'")


function! s:delpair()
	let l:lst = ['""',"''",'{}','[]','()']
	let l:col = col('.')
	let l:line = getline('.')
	let l:chr = l:line[l:col-2 : l:col-1]
	if index(l:lst, l:chr) > -1
		return "\<bs>\<del>"
	else
		let l:chr = l:line[l:col-3:l:col-2]
		if (index(l:lst, l:chr)) > - 1
			return "\<bs>\<bs>"
		endif
		return "\<bs>"
endf

function! s:escapepair(right)
	let l:col = col('.')
	let l:chr = getline('.')[l:col-1]
	if a:right == l:chr
		return "\<right>"
	else
		return a:right

endf

function! s:pairquotes(pair)
	let l:col = col('.')
	let l:line = getline('.')
	let l:chr = l:line[l:col-1]
	if a:pair == l:chr
		return "\<right>"
	else
		return a:pair.a:pair."\<left>"

endf

" Fix unwanted characters when using git fugitive
let &t_TI = ""
let &t_TE = ""
